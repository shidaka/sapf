<?php
if (!defined("SAPRFC_OK")) define("SAPRFC_OK",0);
if (!defined("SAPRFC_ERROR")) define("SAPRFC_ERROR",1);
if (!defined("SAPRFC_APPL_ERROR")) define ("SAPRFC_APPL_ERROR",2);

class SAP
{
    public $serverConnect = "http://10.203.5.48:8080/JCOService/Connect";
    public $serverPOST = "http://10.203.5.48:8080/JCOService/Request";
    public $serverDisconnect = "http://10.203.5.48:8080/JCOService/Disconnect";

    public function connect()
    {
        $connection = array(
            'connection' => array (
                'client' => '500',
                'user' => 'MDEOLIVEIRA',
                'pass' => 'MARCELO0',
                'host' => '10.201.209.22',
                'srvNumber' => '00',
                'lang' => 'EN'
            )
        );

        // Setup cURL
        $ch = curl_init($this->serverConnect);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($connection)
        ));

        // Send the request
        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }

        // Decode the response
        return $connectionID = json_decode($response, TRUE);
    }

    public function callFunction($connectionID, $parameters)
    {
        // Setup cURL
        $ch = curl_init($this->serverPOST);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                //        'Authorization: '.$authToken,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($parameters)
        ));

        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }

        // Decode the response
        $returnSAP = json_decode($response, TRUE);
//		return json_encode($parameters);
        return $returnSAP;
    }

    public function disconnect($connectionID)
    {
        $disconnect = array('connection' => $connectionID);

        // Setup cURL
        $ch = curl_init($this->serverDisconnect);
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                //        'Authorization: '.$authToken,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($disconnect)
        ));

        $response = curl_exec($ch);

        // Check for errors
        if($response === FALSE){
            die(curl_error($ch));
        }

        // Decode the response
        $returnDisconnect = json_decode($response, TRUE);

        if ($returnDisconnect['code']) {
            $this->getStatus('error');
        }

        return $returnDisconnect;
    }
}