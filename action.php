<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 11:00
 */
include ('incHeader.php');
include ("config.php");
?>
<div id="menu">
    <?php include("menu.php"); ?>
</div>
<div id="corpo">
    <?php
    if ($_GET['action'] == "create" || $_POST['action'] == "create") {
       include ("create.php");
    }
    elseif ($_GET['action'] == "modify" || $_POST['action'] == "modify") {
        include ("modify.php");
    }
    elseif ($_GET['action'] == "test" || $_POST['action'] == "test") {
        include ("test.php");
    }
    elseif ($_GET['action'] == "export" || $_POST['action'] == "export") {
        include ("export.php");
    }
    elseif ($_GET['action'] == "view" || $_POST['action'] == "view") {
        include ("view.php");
    }
    else {
        echo "ERR";
        exit();
    }
    ?>
</div>