<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 11:51
 */

include("process.php");
include ("config.php");
$sapf = new process();
if ($_POST['functionName'])  {
    $qSelectAll = "SELECT * FROM functions WHERE id = '".$_POST['functionName']."'";
    $qRequestAll = $dbh->prepare($qSelectAll);
    $qRequestAll->execute();
    $row = $qRequestAll->fetchObject();
    $_POST['import'] = $row->import;
    $_POST['export'] = $row->export;
    $_POST['table'] = $row->tableFunc;
    $result = $sapf->testFunction($_POST, "");
}
?>
<body>
<div id="title">
    <div class="container">
        <form id="contact" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
            <input type="hidden" name="action" value="test">
            <h3>Testes de Funções Cadastradas</h3>
            Selecione abaixo a função a ser testada
            <fieldset>
                <?php
                $qSelectAll = "SELECT * FROM functions";
                $qRequestAll = $dbh->prepare($qSelectAll);
                $qRequestAll->execute();
                ?>
                <select name="functionName" id="functionSelectDropDown">
                    <?php
                    while ($row = $qRequestAll->fetchObject()) {
                        echo '<option value="'.$row->id.'">'.$row->funcao.'</option>';
                    }
                    ?>
                </select>
            </fieldset>
            <fieldset>
                <textarea name="resultFunction" id="resultFunction" placeholder="O retorno do teste da função irá aparecer aqui..." tabindex="5" readonly><?php print_r($result); ?></textarea>
            </fieldset>
            <fieldset>
                <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Testar</button>
            </fieldset>
        </form>
    </div>
</div>
</body>