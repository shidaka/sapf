<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 11:20
 */
?>
<html>
<div class="ms_menu_bar__wrapper">
    <ul class="ms_menu">
        <li class="ms_menu__trigger"><img src="imagens/logo.png"> </li>
    </ul>
</div>
<div class="ms_menu_body__wrapper hide-menu">
    <ul>
        <a href="index.php" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> Principal </li></a>
        <a href="action.php?action=create" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> Criar </li></a>
        <a href="action.php?action=modify" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> Modificar </li></a>
        <a href="action.php?action=test" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> Testar </li></a>
        <a href="action.php?action=export" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> Exportar</li></a>
        <a href="action.php?action=view" ><li class="ms_menu__item"><i class="ms_menu__item-icon fa fa-circle-thin"></i> View</li></a>
    </ul>
</div>
<script src="js/menu.js"></script>
<script src="js/app.js"></script>