<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 11:51
 */


include("process.php");
include ("config.php");
$sapf = new process();

if ($_POST['functionName'])  {
    $result = $sapf->createFullFunction($_POST);
}


?>
<body>
    <div id="title">
        <div class="container">
            <form id="contact" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
                <input type="hidden" name="action" value="create">
                <h3>Cadastro de Novas Funções</h3>
                <h4>Prencha os Campos abaixo</h4>
                Quantidade de IMPORT
                <fieldset>
                    <select name="import" id="importDropDown">
                        <option value="0">00</option>
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                        <option value="4">04</option>
                        <option value="5">05</option>
                        <option value="6">06</option>
                        <option value="7">07</option>
                        <option value="8">08</option>
                        <option value="9">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                    </select>
                </fieldset>
                Quantidade de EXPORT
                <fieldset>
                    <select name="export" id="exportDropDown">
                        <option value="0">00</option>
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                        <option value="4">04</option>
                        <option value="5">05</option>
                        <option value="6">06</option>
                        <option value="7">07</option>
                        <option value="8">08</option>
                        <option value="9">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                    </select>
                </fieldset>
                Quantidade de TABLES
                <fieldset>
                    <select name="table" id="tableDropDown">
                        <option value="0">00</option>
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                        <option value="4">04</option>
                        <option value="5">05</option>
                        <option value="6">06</option>
                        <option value="7">07</option>
                        <option value="8">08</option>
                        <option value="9">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                    </select>
                </fieldset>
                <br />
                <fieldset>
                    <input placeholder="NOME DA FUNCTION" name="functionName" type="text" tabindex="1" required autofocus>
                </fieldset>
                <fieldset id="IMPORT">
                </fieldset>
                <fieldset id="EXPORT">
                </fieldset>
                <fieldset id="TABLE">
                </fieldset>
                <fieldset>
                    <textarea name="resultFunction" id="resultFunction" placeholder="A função pronta irá aparecer aqui..." tabindex="5" readonly><?=$result[0]; ?></textarea>
                </fieldset>
                <fieldset>
                    <textarea name="resultFunction" id="resultFunction" placeholder="O retorno do testa da função irá aparecer aqui..." tabindex="5" readonly><?php print_r($result[1]); ?></textarea>
                </fieldset>
                <fieldset>
                    <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Cadastrar</button>
                </fieldset>
            </form>
        </div>
    </div>
</body>