<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 11:51
 */

include("process.php");
include ("config.php");

?>
<body>
<div id="title">
    <div class="container">
        <form id="contact" action="imagem.php" method="post">
            <input type="hidden" name="action" value="test">
            <h3>Exportar funções cadastradas</h3>
            Selecione abaixo a função a ser exportada
            <fieldset>
                <?php
                $qSelectAll = "SELECT * FROM functions";
                $qRequestAll = $dbh->prepare($qSelectAll);
                $qRequestAll->execute();
                //                $total = $qRequestAll->rowCount();
                ?>
                <select name="functionName" id="functionSelectDropDown">
                    <?php
                    while ($row = $qRequestAll->fetchObject()) {
                        echo '<option value="'.$row->id.'">'.$row->funcao.'</option>';
                    }
                    ?>
                </select>
            </fieldset>
            Selecione abaixo o formato que quer a função
            <fieldset>
                <select name="imageType" id="functionSelect2DropDown">
                    <option value="Excel">Excel</option>
                    <option value="PHP">PHP</option>
                    <option value="img">PNG</option>
                </select>
            </fieldset>
            <fieldset>
                <textarea name="resultFunction" id="resultFunction" placeholder="O retorno do teste da função irá aparecer aqui..." tabindex="5" readonly><?php print_r($result); ?></textarea>
            </fieldset>
            <fieldset>
                <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Testar</button>
            </fieldset>
        </form>
    </div>
</div>
</body>
