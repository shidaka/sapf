<?php
/**
 * Created by Stenner Hidaka.
 * Date: 09/01/2018
 * Time: 10:55
 */

class process {
    function createFunction ($temp, $trava) {
        include ('config.php');
        $varImport="";
        $varFunctionCreated[0] = '<?php
        require_once(\'../SAP.php\'); // Corrigir aqui o caminho do SAP
        $sap = new SAP();
        $resultConnect=$sap->connect($serverConnect);
        $params = array(
            \'connection_id\' => $resultConnect[\'connection_id\'],
            \'repository\' => \'REPOSITORY_NAME\',
            \'function\' => \''.$_POST['functionName'].'\',
        );
        
        
        $params[\'operation\'] = array(
        ';
        if ($_POST['import'] != "0") {
            $varFunctionCreated[0] .=
                '
                array(
                  \'IMPORT\',
                    array(';
            for ($i=0;$i<count($_POST['import']);$i++)  {
                $varFunctionCreated[0] .= "\"".$_POST['import'][$i]."\" => \"".$_POST['importValue'][$i]."\",";
                $dbImport .= "\"".$_POST['import'][$i]."\" => \"".$_POST['importValue'][$i]."\",||";
            }
            $varFunctionCreated[0] .=
                '),
                ),
                ';
        }
        if ($_POST['export'] != "0") {
            for ($i = 0; $i < count($_POST['export'])-1; $i++) {
                $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '",
                ),
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
            if ($_POST['table'] == "0") {
//                $i++;
                 $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '"
                )
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
            else {
                $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '",
                ),
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
        }
        if ($_POST['table'] != "0") {
            for ($i = 0; $i < count($_POST['table'])-1; $i++) {
                $varFunctionCreated[0] .= 'array(
                "TABLE","' . $_POST['table'][$i] . '",
                ),
                ';
                $dbTable .= $_POST['table'][$i]."||";
            }
            $varFunctionCreated[0] .= 'array(
            "TABLE","' . $_POST['table'][$i] . '"
            )
            ';
        }

        $varFunctionCreated[0] .=
            '
         );
        
        $result=$sap->callFunction($resultConnect[\'connection_id\'], $params);
        $resultDisconnect=$sap->connect($resultConnect[\'connection_id\']);
         print_r($result);
        ';



        $varFunctionCreated[1] = $this->testFunction("", $varFunctionCreated[0]);
        if ($trava == "") {
            $this->insertFunction($dbImport, $dbExport, $dbTable);
        }
        return $varFunctionCreated;
    }
    function createFullFunction () {
        include ('config.php');
        $varFunctionCreated[0] = '<?php
        require_once(\'../SAP.php\'); // Corrigir aqui o caminho do SAP
        $sap = new SAP();
        $resultConnect=$sap->connect($serverConnect);
        $params = array(
            \'connection_id\' => $resultConnect[\'connection_id\'],
            \'repository\' => \'REPOSITORY_NAME\',
            \'function\' => \''.$_POST['functionName'].'\',
        );
        
        
        $params[\'operation\'] = array(
        ';
        if ($_POST['import'] != "0") {
            $varFunctionCreated[0] .=
                '
                array(
                  \'IMPORT\',
                    array(';
            for ($i=0;$i<count($_POST['import']);$i++)  {
                $varFunctionCreated[0] .= "\"".$_POST['import'][$i]."\" => \"".$_POST['importValue'][$i]."\",";
                $dbImport .= "\"".$_POST['import'][$i]."\" => \"".$_POST['importValue'][$i]."\",||";
                $dbImportKey .= $_POST['import'][$i]."||";
                $dbImportValue .= $_POST['importValue'][$i]."||";
            }
            $varFunctionCreated[0] .=
                '),
                ),
                ';
        }
        if ($_POST['export'] != "0") {
            for ($i = 0; $i < count($_POST['export'])-1; $i++) {
                $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '",
                ),
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
            if ($_POST['table'] == "0") {
                $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '"
                )
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
            else {
                $varFunctionCreated[0] .= 'array(
                "EXPORT","' . $_POST['export'][$i] . '",
                ),
                ';
                $dbExport .= $_POST['export'][$i]."||";
            }
        }
        if ($_POST['table'] != "0") {
            for ($i = 0; $i < count($_POST['table'])-1; $i++) {
                $varFunctionCreated[0] .= 'array(
                "TABLE","' . $_POST['table'][$i] . '",
                ),
                ';
                $dbTable .= $_POST['table'][$i]."||";
            }
            $varFunctionCreated[0] .= 'array(
            "TABLE","' . $_POST['table'][$i] . '"
            )
            ';
        }

        $varFunctionCreated[0] .=
            '
         );
        
        $result=$sap->callFunction($resultConnect[\'connection_id\'], $params);
        $resultDisconnect=$sap->connect($resultConnect[\'connection_id\']);
         print_r($result);
        ';



        $varFunctionCreated[1] = $this->testFunction("", $varFunctionCreated[0]);

        $this->insertFunction($dbImport, $dbExport,$dbTable);
        $this->insertFullFunction($dbImportKey, $dbImportValue, $dbExport, $dbTable, $varFunctionCreated[0], $varFunctionCreated[1]);

        return $varFunctionCreated;
    }
    function testFunction ($postTest, $varFunctionCreated) {
        include ("config.php");

        if (isset($varFunctionCreated) && $varFunctionCreated != "") {
            $arquivo = "tmp/funcTemp" . date(His) . ".php";
            $fp = fopen($arquivo, "a+");
            fwrite($fp, $varFunctionCreated);
            fclose($fp);
            sleep(5);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $pathTmp . "/" . $arquivo);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            return $output;
        }
        else {
            include ('config.php');
            $varImport="";
            $varFunctionCreated = '<?php
            require_once(\'../SAP.php\'); // Corrigir aqui o caminho do SAP
            $sap = new SAP();
            $resultConnect=$sap->connect($serverConnect);
            $params = array(
                \'connection_id\' => $resultConnect[\'connection_id\'],
                \'repository\' => \'REPOSITORY_NAME\',
                \'function\' => \''.$_POST['functionName'].'\',
            );
            
            
            $params[\'operation\'] = array(
            ';
                if ($_POST['import'] != "0") {
                    $varFunctionCreated .=
                        '
                    array(
                      \'IMPORT\',
                        array(';
                    $importPieces = explode("||", $_POST['import']);
                    for ($i=0;$i<count($importPieces);$i++)  {
                        $varFunctionCreated .= $importPieces[$i]."\r\n";
                    }
                    $varFunctionCreated .=
                        '),
                    ),
                    ';
                }
                if ($_POST['export'] != "0") {
                    $exportPieces = explode("||", $_POST['export']);
                    for ($i = 0; $i < count($exportPieces)-1; $i++) {
                        $varFunctionCreated .= 'array(
                    "EXPORT","' . $exportPieces[$i] . '",
                    ),
                    ';
                    }
                    if ($_POST['table'] == "0") {
    //                $i++;
                        $varFunctionCreated .= 'array(
                    "EXPORT","' . $exportPieces[$i] . '"
                    )
                    ';
                    }
                    else {
                        $varFunctionCreated .= 'array(
                    "EXPORT","' . $exportPieces[$i] . '",
                    ),
                    ';
                        $dbExport .= $_POST['export'][$i]."||";
                    }
                }
                if ($_POST['table'] != "0") {
                    for ($i = 0; $i < count($_POST['table'])-1; $i++) {
                        $varFunctionCreated .= 'array(
                    "TABLE","' . $_POST['table'][$i] . '",
                    ),
                    ';
                        $dbTable .= $_POST['table'][$i]."||";
                    }
                    $varFunctionCreated .= 'array(
                "TABLE","' . $_POST['table'][$i] . '"
                )
                ';
                }

                $varFunctionCreated .=
                    '
             );
            
            $result=$sap->callFunction($resultConnect[\'connection_id\'], $params);
            $resultDisconnect=$sap->connect($resultConnect[\'connection_id\']);
             print_r($result);
            ';

            $arquivo = "tmp/funcTemp" . date(His) . ".php";
            $fp = fopen($arquivo, "a+");
            fwrite($fp, $varFunctionCreated);
            fclose($fp);
            sleep(5);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $pathTmp . "/" . $arquivo);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
            return $output;
        }
    }

    function insertFunction ($dbImport, $dbExport,$dbTable)  {
        include ("config.php");
        $dbh = new PDO('mysql:host='.$servidor_base.';dbname='.$database_base.'', $usuario_base, $senha_base);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh -> setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES UTF-8');

        try {
            $rs = $dbh->prepare('INSERT INTO functions (funcao, import, export, tableFunc) VALUES (\''.$_POST['functionName'].'\',\''.$dbImport.'\',\''.$dbExport.'\',\''.$dbTable.'\')');
            $rs->execute();

        } catch (Exception $e) {
            return $e;
        }
    }
    function insertFullFunction ($dbImportKey, $dbImportValue, $dbExport, $dbTable, $varFont, $varResult)  {
        include ("config.php");
        $dbh = new PDO('mysql:host='.$servidor_base.';dbname='.$database_base.'', $usuario_base, $senha_base);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh -> setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES UTF-8');

        $varFont = str_replace("'", "\'", $varFont);
        $varResult = str_replace("'", "\'", $varResult);

        try {
            $rs = $dbh->prepare('INSERT INTO view (importKey, importValue, exportValue, tableValue, functionComplete, functionResult, functionName) VALUES (\''.$dbImportKey.'\', \''.$dbImportValue.'\',\''.$dbExport.'\',\''.$dbTable.'\', \''.$varFont.'\', \''.$varResult.'\', \''.$_POST['functionName'].'\')');
            $rs->execute();

        } catch (Exception $e) {
            return $e;
        }
    }
    function modifyFunction () {
        $result = var_dump($_POST);
        return TRUE;
    }
    function exportFunction () {
        return TRUE;
    }
    function viewFunction () {
        return TRUE;
    }
    function processDrpdown($selectedVal) {
        echo "Selected value in php ".$selectedVal;
    }
}

