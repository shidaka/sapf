<?php
/**
 * Created by Stenner Hidaka.
 * Date: 12/01/2018
 * Time: 11:35
 */
$servidor_base 	= "localhost";
$database_base 	= "funcoessap";
$usuario_base 	= "root";
$senha_base 	= "";
$dbh = new PDO('mysql:host='.$servidor_base.';dbname='.$database_base.'', $usuario_base, $senha_base);
$dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbh -> setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES UTF-8');

$pathTmp = "http://".$_SERVER['HTTP_HOST']."/SAPF/";